#!/bin/bash

echo "Shutdown server"
kill -9 $(pgrep -f demo-server-1.0.0.jar)
echo "Ok"
